
## site.pp ##

# This file (/etc/puppetlabs/puppet/manifests/site.pp) is the main entry point
# used when an agent connects to a master and asks for an updated configuration.
#
# Global objects like filebuckets and resource defaults should go in this file,
# as should the default node definition. (The default node can be omitted
# if you use the console and don't define any other nodes in site.pp. See
# http://docs.puppetlabs.com/guides/language_guide.html#nodes for more on
# node definitions.)

## Active Configurations ##

# PRIMARY FILEBUCKET
# This configures puppet agent and puppet inspect to back up file contents when
# they run. The Puppet Enterprise console needs this to display file contents
# and differences.

# Define filebucket 'main':
filebucket { 'main':
  server => $::settings::server,
  path   => false,
}

# Make filebucket 'main' the default backup location for all File resources:
File { backup => 'main' }

# Kill deprecation warnings in PE 3.3:
Package { allow_virtual => false }

# DEFAULT NODE
# Node definitions in this file are merged with node data from the console. See
# http://docs.puppetlabs.com/guides/language_guide.html#nodes for more on
# node definitions.

# The default node definition matches any node lacking a more specific node
# definition. If there are no other nodes in this file, classes declared here
# will be included in every node's catalog, *in addition* to any classes
# specified in the console for that node.

 node 'mas.pp.lab' {
}

 node 'agent1.ppt.test' {
}

 node 'cli1.ppt.test' {

  service { 'httpd':
    ensure => 'stopped',
  }

  package { 'httpd':
    ensure => 'absent',
  }


  service { 'ntpd': 
    ensure => 'stopped',
  }

  package { 'ntp':
    ensure => 'absent',
  }
}

  user { 'bobbyDEV':
    ensure           => 'absent',
  }


group { 'DEVusers':
  ensure  => 'absent',
  require => User['bobbyDEV'], 
}

group { 'PRODusers':
  ensure => 'present',
  gid    => '1200',
}


  user { 'bobbyPROD':
    ensure           => 'present',
    comment          => 'PROD USER',
    gid              => '1200',
    groups           => ['PRODusers'],
    home             => '/home/bobby',
    shell            => '/bin/bash',
    uid              => '1837',
  } 
  # This is where you can declare classes for all nodes.
  # Example:
  #   class { 'my_class': }
  #include profiles::jenkins
  #include profiles::puppet_four_changes


# node 'client2.pp.lab' {
#
#  service { 'ntpd':
#    ensure => 'stopped',
#  }
#
#  package { 'ntp':
#    ensure => 'absent',
#  }
#
#  user { 'bobbyDEV':
#    ensure           => 'absent',
#  }
#
#
#group { 'DEVusers':
#  ensure  => 'absent',
#  require => User['bobbyDEV'],
#}
#
#group { 'PRODusers':
#  ensure => 'present',
#  gid    => '1200',
#}
#
#
#  user { 'bobbyPROD':
#    ensure           => 'present',
#    comment          => 'PROD USER',
#    gid              => '1200',
#    groups           => ['PRODusers'],
#    home             => '/home/bobby',
#    shell            => '/bin/bash',
#    uid              => '1837',
#  }
#}


